<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewpoert content="width=device-width, initial-scale=1.0>
        <title>Document</title>
</head>
<body>
        <h1>Lista de paises</h1>
        <table>
            <thead>
                <tr>
                    <th>Pais</th>
                    <th>Capital</th>
                </tr>
            </thead>
            <tbody>
                @foreach($paises as $pais => $infopais)
                    <tr>
                        <td>
                        {{ $pais }}
                        </td>
                        <td>
                        {{ $infopais["capital"] }}
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
</body>
</html>