<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Primera ruta de get

Route::get('hola', function(){
    echo "Hola, estoy en laravel";
});

Route::get('arreglo', function(){
    //Crear un arreglo de estudiantes
    $estudiantes = [ "AD" => "andres",
                    "LA" => "laura",
                    "ANN" => "ana",
                    "RO" => "rodrigo" 
                ];

    //Recorrer un arreglo
    foreach( $estudiantes as $indice => $estudiante ){
        echo "$estudiante tiene indice $indice <hr />";
}
    
});

Route::get('paises', function(){
    //Crear un arreglo con informacion de paises
    $paises = [
        "Colombia" => [
            "capital" => "Bogota" ,
            "moneda" => "peso" ,
            "poblacion" => 50.372
        ],
        "Ecuador" => [
            "capital" => "Quito" ,
            "moneda" => "Dolar" ,
            "poblacion" => 17.517
        ],
        "Brazil" => [
            "capital" => "Brasilia" ,
            "moneda" => "Real" ,
            "poblacion" => 212.216
        ],
        "Bolivia" => [
            "capital" => "La paz" ,
            "moneda" => "Boliviano" ,
            "poblacion" => 11.633
        ]
    ];

    //Recorrer primera dimension
   /* foreach($paises as $pais => $infopais){
        echo "<h2> $pais </h2>";
        echo "capital:" . $infopais["capital"] . "<br />";
        echo "moneda:" . $infopais["moneda"] . "<br />";
        echo "poblacion(en millones de habitantes):" . $infopais["poblacion"] . "<br />";
        echo "<hr /> ";
    }*/

    //mostrar una vista para presentar los paises
    //En MVC yo puedo pasar datos a una vista
    return view('paises')->with("paises" , $paises) ;
});